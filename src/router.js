import Vue from 'vue'
import VueRouter from 'vue-router';

import IndexPage from './pages/Index'
import PostsPage from './pages/Posts'
import PostPage from './pages/Post'
import UserPage from './pages/User'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        component: IndexPage,
        name: 'home'
    },
    {
        path: '/posts',
        name: 'posts',
        component: PostsPage
    },
    {
        path: '/posts/:id',
        name: 'post',
        component: PostPage
    },
    {
        path: '/users/:id',
        name: 'user',
        component: UserPage
    }
]

export default new VueRouter({
    routes
})